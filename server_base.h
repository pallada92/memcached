#include <unordered_set>
#include <mutex>
#include <string>
#include "buffer.h"

class Server {
public:
    Server (std::string port_id, int backlog_size, void (*handler)(int))
        : port_id_(port_id), backlog_size_(backlog_size), handler_(handler) {};
    void start();
    void stop() {
        stop_ = true;
    };
    void add_fd(int fd);

protected:
    std::string port_id_;
    int backlog_size_;
    void (*handler_)(int);
    bool stop_ = false;
    std::mutex fd_set_mutex;
    std::unordered_set<int> fd_set;
};