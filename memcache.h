#include <mutex>
#include <string>
#include "protocol.h"
#include "server_base.h"
#include "cache.cpp"

class Memcache {

    struct Value;
    Cache<std::string, Value> cache;
    Server* server;
    cas_t cas_counter;
    std::mutex mutex;
    exptime_t convert_exptime(exptime_t exptime);
    McResult exec_set_like(McCommand cmd);
    McResult exec_get(McCommand cmd);
    McResult exec_append_like(McCommand cmd);
    McResult exec_touch(McCommand cmd);
    McResult exec_delete(McCommand cmd);

public:

    Memcache(size_t max_size, Server* server);
    McResult execute(McCommand cmd);

};
