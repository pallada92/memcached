#include "gtest/gtest.h"
#include "buffer.h"
#include <unistd.h>
#include <stdio.h>

TEST(SocketRBuffer, buf2) {
    int pipefd[2];
    pipe(pipefd);
    char str1[] = "test string ";
    write(pipefd[1], str1, sizeof(str1) - 1); // without '\0'
    SocketRBuffer rbuf(2, pipefd[0]);
    EXPECT_EQ(rbuf.ReadChar(), 't');
    EXPECT_EQ(rbuf.ReadField(' '), "est");
    EXPECT_NO_THROW(rbuf.ReadCharCheck(' '));
    EXPECT_EQ(rbuf.ReadField(' '), "string");
}

TEST(SocketRBuffer, buf10) {
    int pipefd[2];
    pipe(pipefd);
    const char str1[] = "test ";
    write(pipefd[1], str1, sizeof(str1) - 1); // without '\0'
    SocketRBuffer rbuf(10, pipefd[0]);
    EXPECT_EQ(rbuf.ReadChar(), 't');
    EXPECT_EQ(rbuf.ReadField(' '), "est");
    const char str2[] = "string ";
    write(pipefd[1], str2, sizeof(str2) - 1); // without '\0'
    EXPECT_NO_THROW(rbuf.ReadCharCheck(' '));
    EXPECT_EQ(rbuf.ReadField(' '), "string");
}

TEST(SocketWBuffer, buf2) {
    int pipefd[2];
    pipe(pipefd);
    SocketWBuffer wbuf(2, pipefd[1]);
    wbuf.WriteChar('h');
    wbuf.WriteChar('e');
    wbuf.WriteChar('l');
    char buf[10];
    size_t read_bytes = read(pipefd[0], buf, 10);
    EXPECT_EQ(read_bytes, 2);
    EXPECT_EQ(buf[0], 'h');
    EXPECT_EQ(buf[1], 'e');
    wbuf.Flush();
    read_bytes = read(pipefd[0], buf, 10);
    EXPECT_EQ(read_bytes, 1);
    EXPECT_EQ(buf[0], 'l');
}

TEST(SocketWBuffer, buf10) {
    int pipefd[2];
    pipe(pipefd);
    SocketWBuffer wbuf(10, pipefd[1]);
    wbuf.WriteField("helloworld");
    char buf[10];
    size_t read_bytes = read(pipefd[0], buf, 20);
    EXPECT_EQ(read_bytes, 10);
    EXPECT_EQ(buf[0], 'h');
    EXPECT_EQ(buf[9], 'd');
}
