#include <ctime>
#include "gtest/gtest.h"
#include "cache.cpp"

TEST(Cache, all) {
    Cache<std::string, std::string> cache(10);
    cache.set("1", new std::string("123"), std::time(nullptr) + 100);
    cache.set("2", new std::string("456"), std::time(nullptr) + 100);
    cache.set("3", new std::string("789"), std::time(nullptr) + 100);
    ASSERT_EQ(*cache.get("2", nullptr), "456");
    ASSERT_EQ(*cache.get("1", nullptr), "123");
    ASSERT_EQ(*cache.get("3", nullptr), "789");
    ASSERT_EQ( cache.get("4", nullptr), nullptr);
    exptime_t exptime = std::time(nullptr) + 100;
    cache.set("4", new std::string("012"), exptime);
    exptime_t get_exptime;
    ASSERT_EQ(*cache.get("4", &get_exptime), "012");
    ASSERT_EQ(get_exptime, exptime);
    ASSERT_EQ( cache.get("2", nullptr), nullptr);
    ASSERT_EQ(*cache.get("1", nullptr), "123");
    ASSERT_EQ(*cache.get("3", nullptr), "789");
    ASSERT_EQ(cache.remove("2"), false);
    ASSERT_EQ(cache.remove("3"), true);
    ASSERT_EQ( cache.get("3", nullptr), nullptr);
    cache.set("3", new std::string("345"), 0);
    ASSERT_EQ( cache.get("3", nullptr), nullptr);
    cache.set("3", new std::string("678"), 0);
    ASSERT_EQ(cache.touch("3", std::time(nullptr) + 100), false);
    cache.set("1", new std::string("123"), std::time(nullptr) + 100);
    cache.set("2", new std::string("456"), 0);
    cache.set("3", new std::string("789"), 0);
    cache.set("4", new std::string("012"), 0);
    cache.set("5", new std::string("345"), std::time(nullptr) + 100);
    ASSERT_EQ(*cache.get("1", nullptr), "123");
    ASSERT_EQ(*cache.get("5", nullptr), "345");
}
