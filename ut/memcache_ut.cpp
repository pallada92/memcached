#include "gtest/gtest.h"
#include "memcache.h"

McCommand make_cmd(std::string input) {
    McCommand res;
    StringRBuffer buffer(8, input);
    res.Deserialize(&buffer);
    return res;
}

std::string get_response(McResult result) {
    std::string str_res;
    StringWBuffer buffer(8, &str_res);
    result.Serialize(&buffer);
    buffer.Flush();
    return str_res;
}

TEST(Memcache, get_set_delete) {
    Memcache memcache(10, nullptr);
    std::string res;
    res = get_response(memcache.execute(make_cmd("get 1\r\n")));
    ASSERT_EQ(res, "END\r\n");
    res = get_response(memcache.execute(make_cmd("set 1 0 0 3\r\n123\r\n")));
    ASSERT_EQ(res, "STORED\r\n");
    res = get_response(memcache.execute(make_cmd("get 1\r\n")));
    ASSERT_EQ(res, "VALUE 1 0 3\r\n123\r\nEND\r\n");
    res = get_response(memcache.execute(make_cmd("set 2 0 0 3\r\n456\r\n")));
    res = get_response(memcache.execute(make_cmd("set 3 0 0 3\r\n789\r\n")));
    res = get_response(memcache.execute(make_cmd("set 4 0 0 3\r\n012\r\n")));
    res = get_response(memcache.execute(make_cmd("delete 4\r\n")));
    ASSERT_EQ(res, "DELETED\r\n");
    res = get_response(memcache.execute(make_cmd("get 1 2 3 4\r\n")));
    ASSERT_EQ(res, "VALUE 2 0 3\r\n456\r\nVALUE 3 0 3\r\n789\r\nEND\r\n");
}

TEST(Memcache, add_replace) {
    Memcache memcache(10, nullptr);
    std::string res;
    res = get_response(memcache.execute(make_cmd("add 1 0 0 3\r\n345\r\n")));
    ASSERT_EQ(res, "STORED\r\n");
    res = get_response(memcache.execute(make_cmd("add 1 0 0 3\r\n456\r\n")));
    ASSERT_EQ(res, "NOT_STORED\r\n");
    res = get_response(memcache.execute(make_cmd("replace 1 0 0 3\r\n789\r\n")));
    ASSERT_EQ(res, "STORED\r\n");
    res = get_response(memcache.execute(make_cmd("replace 5 0 0 3\r\n901\r\n")));
    ASSERT_EQ(res, "NOT_STORED\r\n");
}

TEST(Memcache, touch) {
    Memcache memcache(10, nullptr);
    std::string res;
    res = get_response(memcache.execute(make_cmd("add 1 0 0 3\r\n123\r\n")));
    res = get_response(memcache.execute(make_cmd("get 1\r\n123\r\n")));
    ASSERT_EQ(res, "VALUE 1 0 3\r\n123\r\nEND\r\n");
    res = get_response(memcache.execute(make_cmd("touch 1 3592000\r\n")));
    ASSERT_EQ(res, "TOUCHED\r\n");
    res = get_response(memcache.execute(make_cmd("get 1\r\n123\r\n")));
    ASSERT_EQ(res, "END\r\n");
}

TEST(Memcache, append_prepend) {
    Memcache memcache(10, nullptr);
    std::string res;
    res = get_response(memcache.execute(make_cmd("add 1 0 0 3\r\n123\r\n")));
    res = get_response(memcache.execute(make_cmd("prepend 1 0 0 3\r\n789\r\n")));
    res = get_response(memcache.execute(make_cmd("append 1 0 0 3\r\n456\r\n")));
    ASSERT_EQ(res, "STORED\r\n");
    res = get_response(memcache.execute(make_cmd("get 1\r\n")));
    ASSERT_EQ(res, "VALUE 1 0 9\r\n789123456\r\nEND\r\n");
    // res = get_response(memcache.execute(make_cmd("incr 1 10\r\n")));
    // ASSERT_EQ(res, "789123466\r\n");
    // res = get_response(memcache.execute(make_cmd("get 1\r\n")));
    // ASSERT_EQ(res, "VALUE 1 0 9\r\n789123466\r\nEND\r\n");
    // res = get_response(memcache.execute(make_cmd("decr 1 789123460\r\n")));
    // // ASSERT_EQ(res, "6\r\n");
    // res = get_response(memcache.execute(make_cmd("get 1\r\n")));
    // ASSERT_EQ(res, "VALUE 1 0 9\r\n6\r\nEND\r\n");
}

TEST(Memcache, cas_gets) {
    Memcache memcache(10, nullptr);
    std::string res;
    res = get_response(memcache.execute(make_cmd("add 1 0 0 3\r\n123\r\n")));
    res = get_response(memcache.execute(make_cmd("gets 1\r\n")));
    ASSERT_EQ(res, "VALUE 1 0 3 1\r\n123\r\nEND\r\n");
    res = get_response(memcache.execute(make_cmd("cas 1 0 0 3 1\r\n456\r\n")));
    ASSERT_EQ(res, "STORED\r\n");
    res = get_response(memcache.execute(make_cmd("cas 1 0 0 3 1\r\n789\r\n")));
    ASSERT_EQ(res, "EXISTS\r\n");
    res = get_response(memcache.execute(make_cmd("cas 2 0 0 3 1\r\n789\r\n")));
    ASSERT_EQ(res, "NOT_FOUND\r\n");
    res = get_response(memcache.execute(make_cmd("gets 1\r\n")));
    ASSERT_EQ(res, "VALUE 1 0 3 2\r\n456\r\nEND\r\n");
    // res = get_response(memcache.execute(make_cmd("append 1 0 0 3\r\n456\r\n")));
}
