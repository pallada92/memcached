#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <poll.h>
#include <vector>
#include <stdexcept>

#include "server_base.h"

void Server::add_fd(int fd) {
    std::lock_guard<std::mutex> lock(fd_set_mutex);
    printf("adding fd = %d\n", fd);
    fd_set.insert(fd);
}

void Server::start() {
    int status;
    struct addrinfo hints;
    struct addrinfo *servinfo;

    memset(&hints, 0, sizeof(hints)); // make sure the struct is empty
    hints.ai_family = AF_INET;        // IPv4
    hints.ai_socktype = SOCK_STREAM;  // TCP stream sockets
    hints.ai_flags = AI_PASSIVE;      // fill in my IP for me

    if ((status = getaddrinfo(NULL, port_id_.c_str(), &hints, &servinfo)) != 0) {
        perror("  getaddrinfo()");
        exit(1);
    }

    int sockfd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if (sockfd == -1) {
        perror("  socket()");
        exit(1);
    }

    if(bind(sockfd, servinfo->ai_addr, servinfo->ai_addrlen) != 0) {
        perror("  bind()");
        exit(1);
    }

    listen(sockfd, backlog_size_);

    while (!stop_) {

        pollfd *fds;
        size_t fds_size;
        {
            std::lock_guard<std::mutex> lock(fd_set_mutex);
            fds_size = fd_set.size() + 1;
            fds = new pollfd[fds_size];
            fds[0].fd = sockfd;
            fds[0].events = POLLIN;
            int i = 1;
            for (int fd : fd_set)
            {
                fds[i].fd = fd;
                fds[i].events = POLLIN;
                ++i;
            }
        }
        int poll_res = poll(fds, fds_size, 0);
        if (poll_res < 0) {
            perror("  poll()");
            exit(1);
        }
        if (poll_res == 0) {
            usleep(1000);
            continue;
        }
        if (fds[0].revents == POLLIN) {
            struct sockaddr_storage conn_addr;
            socklen_t addr_size = sizeof conn_addr;
            int conn_fd = accept(sockfd, (struct sockaddr *)&conn_addr, &addr_size);
            if (conn_fd == -1) {
                perror("  accept()");
                usleep(100000);
            } else {
                handler_(conn_fd);
            }
        } else {
            for (size_t i = 1; i < fds_size; ++i) {
                if (fds[i].revents == POLLIN) {
                    int fd = fds[i].fd;
                    {
                        std::lock_guard<std::mutex> lock(fd_set_mutex);
                        fd_set.erase(fd);
                    }
                    handler_(fd);
                    break;
                }
            }
        }
        delete[] fds;
    }
    freeaddrinfo(servinfo);
}
