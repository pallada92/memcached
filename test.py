import socket, sys, subprocess, time

suite = [
    ("get abc xyz\r\n", "VALUE 123 127 4\r\ndata\r\nEND\r\n"),
    ("set abc 1 1445128601 5\r\nHello\r\n", "NOT_STORED\r\n"),
    ("delete abc\r\n", "DELETED\r\n"),
    ("add abc 664 0 5\r\nHello\r\n", "STORED\r\n"),
]

def check_responses(suite, port):
    mc = subprocess.Popen(['./server', str(port)])
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    time.sleep(1)
    s.connect(("localhost", port))
    for request, response in suite:
        s.send(request)
        res = s.recv(100)
        if response == res:
            print "ok"
        else:
            print "error"
            print request
            print response
    mc.kill()

if __name__ == "__main__":
    port = int(sys.argv[1])
    check_responses(suite, port)
