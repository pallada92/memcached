#include <string>
#include <vector>
#include <stdexcept>
#include <sstream>
#include <cstdint> 

#include "buffer.h"
#include "protocol.h"

MC_COMMAND CommandName2Code(const std::string& param) {
    if (param == "set") {
        return CMD_SET;
    } else if (param == "add") {
        return CMD_ADD;
    } else if (param == "replace") {
        return CMD_REPLACE;
    } else if (param == "delete") {
        return CMD_DELETE;
    } else if (param == "get") {
        return CMD_GET;
    } else if (param == "gets") {
        return CMD_GETS;
    } else if (param == "delete") {
        return CMD_DELETE;
    } else if (param == "append") {
        return CMD_APPEND;
    } else if (param == "prepend") {
        return CMD_PREPEND;
    } else if (param == "incr") {
        return CMD_INCR;
    } else if (param == "decr") {
        return CMD_DECR;
    } else if (param == "stop") {
        return CMD_STOP;
    } else if (param == "cas") {
        return CMD_CAS;
    } else if (param == "touch") {
        return CMD_TOUCH;
    } else {
        return CMD_UNKNOWN; // throw std::runtime_error("Unknown command"); 
    }
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

void McCommand::Deserialize(RBuffer* buffer) {
    std::string cmd = buffer->ReadField(' ');
    buffer->ReadCharCheck(' ');
    command = CommandName2Code(cmd);
    std::string key;
    size_t n;
    switch (command) {
    case CMD_SET:
    case CMD_ADD:
    case CMD_REPLACE:
    case CMD_APPEND:
    case CMD_PREPEND:
    case CMD_CAS:
        key = buffer->ReadField(' ');
        keys.resize(0);
        keys.push_back(key);
        buffer->ReadCharCheck(' ');
        flags = buffer->ReadUint32();
        buffer->ReadCharCheck(' ');
        exp_time = buffer->ReadUint32();
        buffer->ReadCharCheck(' ');
        n = buffer->ReadUint32();
        if (command == CMD_CAS) {
            buffer->ReadCharCheck(' ');
            cas = buffer->ReadUint64();            
        }
        buffer->ReadCharCheck('\r');
        buffer->ReadCharCheck('\n');
        data = buffer->ReadBytes(n);
        break;
    case CMD_INCR:
    case CMD_DECR:
        key = buffer->ReadField(' ');
        keys.resize(0);
        keys.push_back(key);
        buffer->ReadCharCheck(' ');
        delta_value = buffer->ReadUint64();
        break;
    case CMD_TOUCH:
        key = buffer->ReadField(' ');
        keys.resize(0);
        keys.push_back(key);
        buffer->ReadCharCheck(' ');
        exp_time = buffer->ReadUint32();
        break;
    case CMD_DELETE:
        key = buffer->ReadField('\r');
        keys.resize(0);
        keys.push_back(key);
        break;
    case CMD_GET:
    case CMD_GETS:
        key = buffer->ReadField('\r');
        keys.resize(0);
        split(key, ' ', keys);
        break;
    case CMD_STOP:
        break;
    default:
        break;
    }
    buffer->ReadCharCheck('\r');
    buffer->ReadCharCheck('\n');
}

std::string ResultCode2String(MC_RESULT_CODE code) {
    switch (code) {
    case R_STORED:
        return "STORED";
    case R_NOT_STORED:
        return "NOT_STORED";
    case R_EXISTS:
        return "EXISTS";
    case R_NOT_FOUND:
        return "NOT_FOUND";
    case R_DELETED:
        return "DELETED";
    case R_TOUCHED:
        return "TOUCHED";
    case R_ERROR:
        return "ERROR";
    case R_STOP:
        return "STOPPING SERVER";
    default:
        throw std::runtime_error("Unknown result code");
    }
}

void McValue::Serialize(WBuffer* buffer) const {
    buffer->WriteField(key_, ' ');
    buffer->WriteUint32(flags_);
    buffer->WriteChar(' ');
    buffer->WriteUint32(data_.size());
    if (cas != 0) {
        buffer->WriteChar(' ');
        buffer->WriteUint64(cas);
    }
    buffer->WriteChar('\r');
    buffer->WriteChar('\n');
    buffer->WriteBytes(data_);
    buffer->WriteChar('\r');
    buffer->WriteChar('\n');
}

void McValue::Serialize_num(WBuffer* buffer) const {
    buffer->WriteBytes(data_);
}

void McResult::Serialize(WBuffer* buffer) const {
    switch(type_) {
    case RT_CLIENT_ERROR:
        buffer->WriteField("CLIENT_ERROR", ' ');
        buffer->WriteField(error_message_);
        break;
    case RT_SERVER_ERROR:
        buffer->WriteField("SERVER_ERROR", ' ');
        buffer->WriteField(error_message_);
        break;
    case RT_NUMBER:
        values_[0].Serialize_num(buffer);
        break;
    case RT_VALUE:
        for (McValue value : values_) {
            buffer->WriteField("VALUE", ' ');
            value.Serialize(buffer);
        }
        buffer->WriteField("END");
        break;
    case RT_CODE:
        buffer->WriteField(ResultCode2String(code_));
        break;
    default:
        throw std::runtime_error("Unknown result type"); 
    }
    buffer->WriteChar('\r');
    buffer->WriteChar('\n');
}
