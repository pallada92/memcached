#include <string>
#include <stdio.h>
#include <cstdint>
#include <vector>
#include <ctime>
#include <limits>
#include <set>
#include <condition_variable>
#include <stdlib.h>
#include <iterator>

#include "protocol.h"

#include "memcache.h"

struct Memcache::Value {
    std::vector<char> data;
    flags_t flags;
    cas_t cas;
    Value(std::vector<char> data, flags_t flags, cas_t cas)
        : data(data), flags(flags), cas(cas) {};
    size_t size() {
        return data.size();
    }
};

exptime_t Memcache::convert_exptime(exptime_t exptime) {
    if (exptime > 60*60*24*30) {
        return exptime;
    } else if (exptime == 0) {
        return std::numeric_limits<exptime_t>::max();
    } else {
        return exptime + std::time(nullptr);
    }        
}

McResult Memcache::exec_set_like(McCommand cmd) {
    Value* val = new Value(std::move(cmd.data), cmd.flags, cas_counter++);
    exptime_t exptime = convert_exptime(cmd.exp_time);
    if (cmd.command == CMD_ADD) {
        if (cache.get(cmd.keys[0], nullptr) != nullptr) {
            return McResult(R_NOT_STORED);
        }
    } else if (cmd.command == CMD_REPLACE) {
        if (cache.get(cmd.keys[0], nullptr) == nullptr) {
            return McResult(R_NOT_STORED);
        }
    } else if (cmd.command == CMD_CAS) {
        Value* val = cache.get(cmd.keys[0], nullptr);
        if (val != nullptr) {
            if (val->cas != cmd.cas) {
                return McResult(R_EXISTS);
            }
        } else {
            return McResult(R_NOT_FOUND);
        }
    }
    bool res = cache.set(cmd.keys[0], val, exptime);
    if (res) {
        return McResult(R_STORED);
    } else {
        return McResult(R_NOT_STORED);
    }
}

McResult Memcache::exec_get(McCommand cmd) {
    std::vector<McValue> res;
    for (std::string key : cmd.keys) {
        Value* val = cache.get(key, nullptr);
        if (val == nullptr) continue;
        if (cmd.command == CMD_GET) {
            res.emplace_back(key, val->flags, val->data);
        } else if (cmd.command == CMD_GETS) {
            res.emplace_back(key, val->flags, val->data, val->cas);            
        }
    }
    return McResult(res, RT_VALUE);
}

McResult Memcache::exec_touch(McCommand cmd) {
    if (cache.touch(cmd.keys[0], convert_exptime(cmd.exp_time))) {
        return McResult(R_TOUCHED);
    } else {
        return McResult(R_NOT_FOUND);
    }
}

McResult Memcache::exec_delete(McCommand cmd) {
    if (cache.remove(cmd.keys[0])) {
        return McResult(R_DELETED);
    } else {
        return McResult(R_NOT_FOUND);
    }
}

McResult Memcache::exec_append_like(McCommand cmd) {
    exptime_t old_exptime = 0;
    Value* old_val = cache.get(cmd.keys[0], &old_exptime);
    if (old_val == nullptr) {
        return McResult(R_NOT_FOUND);
    }
    std::vector<char> new_data;
    if (cmd.command == CMD_APPEND) {
        if (old_val != nullptr) {
            std::copy(old_val->data.begin(), old_val->data.end(), std::back_inserter(new_data));
        }
        std::copy(cmd.data.begin(), cmd.data.end(), std::back_inserter(new_data));
    } else if (cmd.command == CMD_PREPEND) {
        std::copy(cmd.data.begin(), cmd.data.end(), std::back_inserter(new_data));
        if (old_val != nullptr) {
            std::copy(old_val->data.begin(), old_val->data.end(), std::back_inserter(new_data));
        }
    } else if (cmd.command == CMD_INCR || cmd.command == CMD_DECR) {
        num_val_t old_num = 0;
        if (old_val != nullptr) {
            char* old_num_ptr = old_val->data.data();
            old_num = strtoull(old_num_ptr, nullptr, 10);
        }
        num_val_t new_num = 0;
        if (cmd.command == CMD_INCR) {
            new_num = old_num + cmd.delta_value;
        } else if (cmd.command == CMD_DECR) {
            if (old_num < cmd.delta_value) {
                new_num = 0;
            } else {
                new_num = old_num - cmd.delta_value;
            }
        }
        char buf[64];
        sprintf(buf, "%lu", new_num);
        std::string buf_str(buf);
        std::copy(buf_str.begin(), buf_str.end(), std::back_inserter(new_data));
    }
    Value* new_val = new Value(new_data, old_val->flags, cas_counter++);
    bool res = cache.set(cmd.keys[0], new_val, old_exptime);
    if (res) {
        if (cmd.command == CMD_INCR || cmd.command == CMD_DECR) {
            std::vector<McValue> values;
            McValue val("", 0, std::vector<char>());
            values.push_back(val);
            return McResult(values, RT_NUMBER);
        } else {
            return McResult(R_STORED);
        }
    } else {
        return McResult(R_NOT_STORED);
    }
}

Memcache::Memcache(size_t max_size, Server* server)
: cache(max_size), server(server), cas_counter(1) {};

McResult Memcache::execute(McCommand cmd) {
    std::lock_guard<std::mutex> lock(mutex);
    switch (cmd.command) {
    case CMD_SET:
    case CMD_ADD:
    case CMD_REPLACE:
    case CMD_CAS:
        return exec_set_like(cmd);
        break;
    case CMD_GET:
    case CMD_GETS:
        return exec_get(cmd);
    case CMD_APPEND:
    case CMD_PREPEND:
    case CMD_INCR:
    case CMD_DECR:
        return exec_append_like(cmd);
        break;
    case CMD_TOUCH:
        return exec_touch(cmd);
        break;
    case CMD_DELETE:
        return exec_delete(cmd);
        break;
    case CMD_STOP:
        server->stop();
        printf("stop!");
        return McResult(R_STOP);
    default:
        return McResult(R_ERROR);
    }
}
