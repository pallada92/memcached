#include <stdlib.h>
#include <exception>
#include <cstdint>
#include <mutex>
#include <map>
#include <utility>
#include <unordered_map>
#include <list>
#include <limits>
#include <ctime>

typedef std::uint32_t exptime_t;

template <typename Key, typename Val>
class Cache {

    class Entry;

    std::mutex cache_mutex;
    std::unordered_map<Key, Entry*> key_map;
    std::multimap<exptime_t, Entry*> exptime_map;
    std::list<Entry*> last_used_list;
    size_t cache_size = 0;
    size_t cache_max_size;

    struct Entry {
        typename std::multimap<exptime_t, Entry*>::iterator exptime_iter;
        typename std::list<Entry*>::iterator last_used_iter;
        Key key;
        Val* val;
        exptime_t exptime;
        Entry(Key key, Val* val, exptime_t exptime)
            : key(key), val(val), exptime(exptime) {};
        ~Entry() {
            delete val;
        }
    };

    void add_entry(Key key, Val* val, exptime_t exptime) {
        Entry* entry = new Entry(key, val, exptime);
        // caller should delete key_map[key], if it existed before
        key_map[entry->key] = entry;
        entry->exptime_iter = exptime_map.insert(std::pair<exptime_t, Entry*>(entry->exptime, entry));
        last_used_list.push_front(entry);
        entry->last_used_iter = last_used_list.begin();
        cache_size += entry->val->size();
    }

    void remove_entry(Entry* entry) {
        key_map.erase(entry->key);
        exptime_map.erase(entry->exptime_iter);
        last_used_list.erase(entry->last_used_iter);
        cache_size -= entry->val->size();
        delete entry;
    }

    void update_last_used(Entry* entry) {
        last_used_list.erase(entry->last_used_iter);
        last_used_list.push_front(entry);
        entry->last_used_iter = last_used_list.begin();
    }

    void update_exptime(Entry* entry, exptime_t new_exptime) {
        entry->exptime = new_exptime;
        exptime_map.erase(entry->exptime_iter);
        entry->exptime_iter = exptime_map.insert(std::pair<exptime_t, Entry*>(new_exptime, entry));
    }

    bool cleanup(int free_space) {
        while (1) {
            auto min_exptime = exptime_map.begin();
            if (min_exptime == exptime_map.end()) break;
            if (min_exptime->first < std::time(nullptr)) {
                remove_entry(min_exptime->second);
            } else {
                break;
            }
        }
        while (cache_size + free_space > cache_max_size) {
            auto last_used = last_used_list.rbegin();
            if (last_used == last_used_list.rend()) break;
            remove_entry(*last_used);
        }
        return cache_size + free_space < cache_max_size;
    }

    Entry* find_entry(Key key) {
        auto res = key_map.find(key);
        if (res == key_map.end()) {
            return nullptr;
        }
        Entry* entry = res->second;
        if (entry->exptime <= std::time(nullptr)) {
            remove_entry(entry);
            return nullptr;
        }
        return entry;
    }

public:

    Cache(size_t cache_max_size) : cache_max_size(cache_max_size) {}

    Val* get(Key key, exptime_t* exptime) {
        std::lock_guard<std::mutex> lock(cache_mutex);
        Entry* entry = find_entry(key);
        if (entry == nullptr) {
            return nullptr;
        }
        update_last_used(entry);
        if (exptime != nullptr) {
            *exptime = entry->exptime;
        }
        return entry->val;
    }

    bool set(Key key, Val* val, exptime_t exptime) {
        std::lock_guard<std::mutex> lock(cache_mutex);
        if (!cleanup(val->size())) {
            delete val;
            return true;
        }
        Entry* entry = find_entry(key);
        if (entry != nullptr) {
            remove_entry(entry);
        }
        add_entry(key, val, exptime);
        return true;
    }

    bool remove(Key key) {
        std::lock_guard<std::mutex> lock(cache_mutex);
        Entry* entry = find_entry(key);
        if (entry == nullptr) {
            return false;
        } else {
            remove_entry(entry);
            return true;
        }
    }

    bool touch(Key key, exptime_t exptime) {
        std::lock_guard<std::mutex> lock(cache_mutex);
        Entry* entry = find_entry(key);
        if (entry == nullptr) {
            return false;
        } else {
            update_exptime(entry, exptime);
            update_last_used(entry);
            return true;
        }
    }

};

// int main() {
//     const size_t cache_size   = 10000000;
//     const size_t max_index    = 100;
//     const size_t trials_count = 10000;
//     Cache<std::string, std::string> cache(cache_size);
//     printf("started\n");
//     for (size_t i=0; i<trials_count; i++) {
//         int idx;
//         idx = random() % max_index;
//         std::string* val = new std::string("1234567890");
//         cache.set(std::to_string(idx), val, std::time(nullptr) + 1000);
//         idx = random() % max_index;
//         cache.get(std::to_string(idx), nullptr);
//     }
//     printf("finished\n");
// }
