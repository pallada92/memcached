#pragma once

#include "buffer.h"

#include <cstdint> 
#include <vector>

typedef std::uint64_t flags_t;
typedef std::uint64_t cas_t;
typedef std::uint64_t num_val_t;

enum MC_COMMAND {
    CMD_UNKNOWN,
    CMD_SET,
    CMD_ADD,
    CMD_REPLACE,
    CMD_GET,
    CMD_GETS,
    CMD_DELETE,
    CMD_APPEND,
    CMD_PREPEND,
    CMD_INCR,
    CMD_DECR,
    CMD_STOP,
    CMD_CAS,
    CMD_TOUCH,
    MC_COMMANDS_NUMBER  // always goes last
};

MC_COMMAND CommandName2Code(const std::string& param);

enum MC_RESULT_CODE {
    R_STORED,
    R_NOT_STORED,
    R_EXISTS,
    R_NOT_FOUND,
    R_TOUCHED,
    R_DELETED,
    R_ERROR,
    R_STOP
};

std::string ResultCode2String(MC_RESULT_CODE code);

class McValue {
private:
    std::string key_;
    int flags_;
    std::vector<char> data_;
    cas_t cas;

public:
    McValue(std::string key, int flags, std::vector<char> data_block, cas_t cas)
        : key_(key)
        , flags_(flags)
        , data_(data_block)
        , cas(cas)
    {}

    McValue(std::string key, int flags, std::vector<char> data_block)
        : key_(key)
        , flags_(flags)
        , data_(data_block)
        , cas(0)
    {}

    void Serialize(WBuffer* buffer) const;

    void Serialize_num(WBuffer* buffer) const;

};

struct McCommand {
    MC_COMMAND command = CMD_UNKNOWN;
    std::vector<std::string> keys;
    flags_t flags = 0;
    time_t exp_time = 0;
    std::vector<char> data;
    num_val_t delta_value;
    cas_t cas;

    void Deserialize(RBuffer* buffer);
};

enum RESULT_TYPE {
    RT_CODE,
    RT_VALUE,
    RT_CLIENT_ERROR,
    RT_SERVER_ERROR,
    RT_NUMBER
};

class McResult {

    RESULT_TYPE type_;
    std::vector<McValue> values_;
    std::string error_message_;
    MC_RESULT_CODE code_;

public:
    McResult(MC_RESULT_CODE result_code)
        : type_(RT_CODE)
        , code_(result_code)
    {}
    McResult(std::vector<McValue> values, RESULT_TYPE res_type)
        : type_(res_type)
        , values_(values)
    {}
    McResult(const std::string& error_message, RESULT_TYPE res_type)
        : type_(res_type)
        , error_message_(error_message)
    {}

    void Serialize(WBuffer* buffer) const;
};


