#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <vector>
#include <queue>
#include <map>
#include <stdexcept>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "buffer.h"
#include "protocol.h"
#include "memcache.h"

#define BUFFER_SIZE 4096
#define BACKLOG_SIZE 20

typedef std::map<std::string, std::string> config_t;

void parse_config(const char* path, config_t &res) {
    FILE *f = fopen(path, "r");
    char lhs[50], rhs[50];
    while (fscanf(f, "%s = %s\n", lhs, rhs) != EOF) {
        res[lhs] = rhs;
    }
}

template <typename T> // T should be a simple type
class TS_queue {
    std::condition_variable queue_condvar_;
    std::mutex queue_mutex_;
    std::queue<int> queue_;
public:
    size_t size() {
        std::lock_guard<std::mutex> guard(queue_mutex_);
        return queue_.size();
    }
    T pop() {
        std::unique_lock<std::mutex> queue_lock(queue_mutex_);
        while (queue_.empty()) {
            queue_condvar_.wait(queue_lock);
        }
        T res = queue_.front();
        queue_.pop();
        return res;
    }
    void push(T elem) {
        {
            std::lock_guard<std::mutex> guard(queue_mutex_);
            queue_.push(elem);
        }
        queue_condvar_.notify_one();
    }
};

TS_queue<int> task_queue;

Memcache* memcache;
Server* server;

void worker_thread_func() {
    while (1) {
        int fd = task_queue.pop();
        SocketRBuffer rbuf(BUFFER_SIZE, fd);
        SocketWBuffer wbuf(BUFFER_SIZE, fd);
        McCommand cmd;
        try {
            cmd.Deserialize(&rbuf);
        } catch (std::runtime_error& e) {
            McResult(e.what(), RT_CLIENT_ERROR).Serialize(&wbuf);
        }
        try {
            McResult res = memcache->execute(cmd);
            res.Serialize(&wbuf);
            wbuf.Flush();
            server->add_fd(fd);
        } catch (std::runtime_error& e) {
            McResult(e.what(), RT_SERVER_ERROR).Serialize(&wbuf);
        }
    }
}

void stat_thread_func() {
    while (1) {
        sleep(1);
        printf("Task queue size = %lu\n", task_queue.size());
    }
}

void handler(int fd) {
    printf("Pushing fd = %d\n", fd);
    task_queue.push(fd);
}

int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: server PATH_TO_CONFIG\n");
        exit(1);
    }
    config_t config = {
        {"port", "1234"},
        {"threads", "10"},
        {"cache_size_mb", "10"},
    };
    parse_config(argv[1], config);
    size_t cache_size = strtoul(config["cache_size_mb"].c_str(), nullptr, 10) * 1024 * 1024;
    server = new Server(config["port"], BACKLOG_SIZE, handler);
    memcache = new Memcache(cache_size, server);
    std::thread stat_thread(stat_thread_func);
    std::vector<std::thread> worker_threads;
    int threads_num = atoi(config["threads"].c_str());
    for (int i = 0; i < threads_num; ++i)
    {
        worker_threads.push_back(std::thread(worker_thread_func));
    }
    server->start();
}
